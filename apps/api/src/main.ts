/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { INestApplication, Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';

import { AppModule } from './app/app.module';

import * as proxy from 'express-http-proxy';

function setupProxy(app: INestApplication) {
  console.log('Setting up proxies from environment variables');
  Object.keys(process.env)
  .map(key => key.match(/^NODE_PROXY__(.*)/))
  .filter(Boolean)
  .forEach(match => {
    const [ key, envPath ] = match;
    if (!envPath) return console.warn(`Could not extract path from ${key}`);
    const path = `/${envPath.toLowerCase().trim()}`;
    const target = process.env[key];
    console.log(`${path} => ${target}`);
    app.use(path, proxy(target));
  });
}

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  setupProxy(app);
  const globalPrefix = '';
  app.setGlobalPrefix(globalPrefix);
  const port = process.env.PORT || 3333;
  await app.listen(port, () => {
    Logger.log('Listening at http://localhost:' + port + '/' + globalPrefix);
  });
}

bootstrap();
