FROM whatshiywl/nrwl-base:1.0.0 AS build

WORKDIR /prod
COPY package.json package-lock.json decorate-angular-cli.js ./
RUN npm i --production

WORKDIR /build
COPY package.json package-lock.json decorate-angular-cli.js ./
RUN npm i
COPY . .
RUN node node_modules/.bin/ng build front --aot && \
    node node_modules/.bin/ng build api --aot

FROM node:16-slim
WORKDIR /app
COPY --from=build /prod/node_modules node_modules
COPY --from=build /build/dist dist
ENTRYPOINT [ "node", "dist/apps/api/main.js" ]
